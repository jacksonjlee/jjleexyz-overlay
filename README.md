# A Personal Overlay for my Gentoo Systems

Feel free to use this if you feel so inclined.

## Installation
- If using eselect-repository:
`eselect repository add jjleexyz git https://gitlab.com/jacksonjlee/jjleexyz-overlay.git`
- If using repos.conf
```
[jjleexyz]
location = /var/db/repos/jjleexyz
sync-type = git
sync-uri = https://gitlab.com/jacksonjlee/jjleexyz-overlay.git
```
